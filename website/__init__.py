from flask import Flask, render_template

def create_app():
	app = Flask(__name__)

	@app.route("/")
	def main():
		return render_template("/main.html")

	@app.route("/sobremesa/lowcost")
	def low_cost():
		return render_template("/sobremesa/low_cost.html")
	@app.route("/sobremesa/gaming")
	def gaming():
		return render_template("/sobremesa/gaming.html")

	@app.route("/portatil/lowcost")
	def low_cost_portatil():
		return render_template("/portatiles/low_cost_portatil.html")
	@app.route("/portatil/gaming")
	def gaming_portatil():
		return render_template("/portatiles/gaming_portatil.html")

	@app.route("/componentes/procesadores")
	def procesadores():
		return render_template("/componentes/procesadores.html")


	return app